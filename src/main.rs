use clap::{Parser, Subcommand};
use std::path::PathBuf;
use std::collections::HashMap;
use rusqlite::{Connection, Result};

#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
#[clap(propagate_version = true)]
struct Cli{
    #[clap(subcommand)]
    command: Commands,
    /// override data dir
    #[clap(short, value_parser, value_name = "~/.namepet")]
    directory: Option<PathBuf>,
    /// override 'yourself' key
    #[clap(short, action)]
    kill: bool,
    /// export hex data  (make output work with `pet adopt`)
    #[clap(short, action)]
    xhex: bool,
    /// plain-language namecard
    #[clap(short, value_parser, value_name = "en")]
    language: Option<String>,
}

#[derive(Subcommand)]
enum Commands {
    /// set [key,val]             (according to yourself)
    Set {
        #[clap(value_parser)]
        key: String,
        val: String,
    },
    /// get [key, val]            (according to yourself)
    Get {
        #[clap(value_parser)]
        key: String,
    },
    /// get all [key,val] by val  (according to yourself)
    Inv {
        #[clap(value_parser)]
        val: String,
    },
    /// get all [key, val] by key (according to anyone)
    Ask {
        #[clap(value_parser)]
        key: String,
    },
    /// get all [key,val] by val  (according to anyone)
    All {
        #[clap(value_parser)]
        val: String,
    },
    /// show 'yourself' pubkey
    Pubkey,
    /// revoke 'yourself' keypair
    Revoke,
    /// check for revoked / ambiguous petnames
    Audit,
    /// read namecard, then give own petnames
    Adopt,
}

#[derive(Debug)]
struct PetName {
    name: String,
    petname: String,
    owner: String
}

fn main() -> Result<()> {
    let cli = Cli::parse();

    let conn = Connection::open("petnames.sqlite")?;

    conn.execute(
        "CREATE TABLE if not exists petnames( id integer primary key, name text not null unique, petname text not null unique, owner text not null)",
        (),
    )?;

    let _init = PetName {
        name: "initname".to_string(),
        petname: "zed".to_string(),
        owner: "self".to_string()
    };

    let update = |p: &PetName| -> Result<()> {
        conn.execute(
            "UPDATE petnames SET petname=?2 WHERE name=?1 AND owner=?3",
            (&p.name, &p.petname, &p.owner),
        )?;
        Ok(())
    };

    let insert = |p: &PetName| -> Result<()> {
        conn.execute(
            "INSERT INTO petnames (name, petname, owner) VALUES (?1, ?2, ?3)",
            (&p.name, &p.petname, &p.owner),
        )?;
        Ok(())
    };

    let query_name = |n, p, o| -> Result<Vec<PetName>> {
        let mut stmt = conn.prepare("SELECT id, name, petname, owner FROM petnames WHERE name LIKE ? AND petname LIKE ? AND owner LIKE ?")?;
        let mut rows = stmt.query(rusqlite::params![n, p, o])?;
        let mut names = Vec::new();
        while let Some(row) = rows.next()? {
            names.push(PetName {
                name: row.get(1)?,
                petname: row.get(2)?,
                owner: row.get(3)?,
            });
        }

        Ok(names)
    };


    match &cli.command {
        Commands::Set { key, val } => {
            let p = PetName {
                name: key.to_string(),
                petname: val.to_string(),
                owner: "self".to_string()
            };
            update(&p);
            insert(&p);
            println!("{:?}", &p);
        },
        Commands::Get { key } => {
            let n = query_name(key, "%", "self");
            println!("vec: {:?}", n);
        },
        Commands::Inv { val } => {
            let names = query_name(&"%".to_string(), val, "self");
            println!("res: {:?}", names)
        },
        Commands::Ask { key } => {
            let names = query_name(key, "%", "%");
            println!("vec : {:?}", names)
        },
        Commands::All { val } => {
            let names = query_name(&"%".to_string(), val, "%");
            println!("vec :{:?}", names)
        },
        Commands::Pubkey  => {
            println!("PUBKEY")
        },
        Commands::Revoke  => {
            println!("REVOKE")
        },
        Commands::Audit  => {
            println!("AUDIT")
        },
        Commands::Adopt  => {
            println!("ADOPT")
        },
    }

    if let Some(directory) = cli.directory.as_deref() {
        println!("Value for directory is {}", directory.display());
    }
    println!("k: {:?}", cli.kill);
    println!("language: {:?}", cli.language);
    println!("hex: {:?}", cli.xhex);
    Ok(())
}
